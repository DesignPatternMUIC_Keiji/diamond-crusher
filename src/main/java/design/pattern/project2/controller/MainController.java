package design.pattern.project2.controller;

import design.pattern.project2.constant.ComponentName;
import design.pattern.project2.gui.component.GamePanel;
import design.pattern.project2.gui.component.MainFrame;
import design.pattern.project2.gui.component.ScorePanel;
import design.pattern.project2.gui.component.TimerPanel;
import design.pattern.project2.listener.EventBusListener.*;
import design.pattern.project2.listener.NotifyEventBusListener.CustomizeGameButtonListener;
import design.pattern.project2.listener.NotifyEventBusListener.NewGameButtonListener;
import design.pattern.project2.model.Board;
import design.pattern.project2.model.ClickCounter;
import design.pattern.project2.service.BoardService;
import design.pattern.project2.service.DimensionCalculationService;
import design.pattern.project2.service.impl.BoardServiceImpl;
import design.pattern.project2.service.impl.DimensionCalculationServiceImpl;
import design.pattern.project2.singleton.EventBus;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class MainController {
    private MainFrame mainFrame;
    private ScorePanel scorePanel;
    private TimerPanel timerPanel;
    private GamePanel gamePanel;
    private Map<String,JMenuItem> menuItemHashMap = new HashMap<>();
    private DimensionCalculationService calculationService = new DimensionCalculationServiceImpl();
    private BoardService boardService = new BoardServiceImpl();

    public MainController() {
        mainFrame = new MainFrame();
        mainFrame.setTitle("Game");
        JMenuBar menubar = new JMenuBar();
        JMenu menu = new JMenu("Options");
        JMenuItem newGameItem = new JMenuItem("New Game");
        JMenuItem customizeGameItem = new JMenuItem("Customize Dimensions");

        menuItemHashMap.put("newgame",newGameItem);
        menuItemHashMap.put("customize",customizeGameItem);
        menu.add(newGameItem);
        menu.add(customizeGameItem);
        menubar.add(menu);
        mainFrame.setJMenuBar(menubar);

        setUpGame(mainFrame.getRows(),mainFrame.getColumns());

        newGameItem.addActionListener(new NewGameButtonListener());
        customizeGameItem.addActionListener(new CustomizeGameButtonListener());
        EventBus.register(new NewGameListener(ComponentName.NEWGAME,this));
        EventBus.register(new CustomizeGameListener(ComponentName.CUSTOMIZEGAME,this));
        mainFrame.setVisible(true);
    }
    public void newGame(){
        setUpGame(this.gamePanel.getRows(), this.gamePanel.getColumns());
    }

    public void setUpGame(int rows, int columns){
        int[] mainFrameDimensions = calculationService.calculateMainFrameDimensions(rows,columns);

        //Size setting of the frame
        this.mainFrame.setRows(rows);
        this.mainFrame.setColumns(columns);
        this.mainFrame.setSize(mainFrameDimensions[0],mainFrameDimensions[1]);
        this.mainFrame.getJMenuBar().setSize(calculationService.calculateMenuBarDimensions(mainFrame.getColumns())[0],
                calculationService.calculateMenuBarDimensions(mainFrame.getColumns())[1]);

        //Adding GamePanel into the mainframe
        if (this.gamePanel != null) {
            System.out.println("removing items");
            this.mainFrame.remove(this.gamePanel);
            this.mainFrame.remove(this.scorePanel);
            this.mainFrame.remove(this.timerPanel);
            System.out.println("unregistering items");
            EventBus.unregister(ComponentName.ANIMATE);
            EventBus.unregister(ComponentName.SWAP);
            EventBus.unregister(ComponentName.STOP);
            EventBus.unregister(ComponentName.GAMEOVER);
            EventBus.unregister(ComponentName.ADDSCORE);
            System.out.println("Finished");
        }

        Board board = new Board(rows,columns);

        this.gamePanel = new GamePanel(rows,columns,new ClickCounter());
        this.scorePanel = new ScorePanel(calculationService.calculateScorePanelDimensions()[0], calculationService.calculateScorePanelDimensions()[1]);
        this.timerPanel = new TimerPanel(calculationService.calculateTimerDimensions()[0], calculationService.calculateTimerDimensions()[1]);

        this.mainFrame.setLayout(new FlowLayout());

        this.mainFrame.add(this.scorePanel);
        this.mainFrame.add(this.gamePanel);
        this.mainFrame.add(this.timerPanel);
        this.gamePanel.render(this.boardService.translateBoard(board));

        EventBus.register(new AnimatorListener(ComponentName.ANIMATE, this.gamePanel));
        EventBus.register(new UpdateBoardListener(ComponentName.SWAP,this.gamePanel,board));
        EventBus.register(new StopAnimationListener(ComponentName.STOP,this.gamePanel));
        EventBus.register(new GameOverListener(ComponentName.GAMEOVER,this,this.scorePanel));
        EventBus.register(new AddScoreListener(ComponentName.ADDSCORE, this.scorePanel));
    }
}
