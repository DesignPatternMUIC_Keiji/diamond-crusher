package design.pattern.project2.listener.EventBusListener;

import design.pattern.project2.gui.component.ScorePanel;

public class AddScoreListener extends GenericListener{
    private ScorePanel scorePanel;

    public AddScoreListener(String componentName, ScorePanel scorePanel) {
        super(componentName);
        this.scorePanel = scorePanel;
    }

    @Override
    public void update(Object item) {
        this.scorePanel.update((int) item);
    }
}
