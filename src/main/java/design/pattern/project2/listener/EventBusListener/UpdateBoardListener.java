package design.pattern.project2.listener.EventBusListener;

import design.pattern.project2.constant.ComponentName;
import design.pattern.project2.gui.component.GamePanel;
import design.pattern.project2.model.Board;
import design.pattern.project2.model.Command;
import design.pattern.project2.service.GameService;
import design.pattern.project2.service.impl.GameServiceImpl;
import design.pattern.project2.singleton.EventBus;

import java.util.List;

public class UpdateBoardListener extends GenericListener{
    private GamePanel gamePanel;
    private Board board;
    private GameService gameService = new GameServiceImpl();

    public UpdateBoardListener(String componentName, GamePanel gamePanel, Board board) {
        super(componentName);
        this.gamePanel = gamePanel;
        this.board = board;
    }

    @Override
    public void update(Object item) {
        List<Integer[]> positionsClicked = this.gamePanel.getPositionsClicked();
        List<Command> commands = this.gameService.switchNode(this.board
                ,positionsClicked.get(0)[0]
                ,positionsClicked.get(0)[1]
                ,positionsClicked.get(1)[0]
                ,positionsClicked.get(1)[1]);
        this.board = commands.get(commands.size()-1).getBoard();
        EventBus.notify(ComponentName.ANIMATE,commands);
    }
}
