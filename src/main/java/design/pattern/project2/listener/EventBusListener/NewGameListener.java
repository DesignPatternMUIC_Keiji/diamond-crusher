package design.pattern.project2.listener.EventBusListener;

import design.pattern.project2.controller.MainController;

public class NewGameListener extends GenericListener{
    MainController mainController;
    public NewGameListener(String componentName, MainController controller) {
        super(componentName);
        mainController = controller;
    }

    @Override
    public void update(Object item) {
        mainController.newGame();
    }
}
