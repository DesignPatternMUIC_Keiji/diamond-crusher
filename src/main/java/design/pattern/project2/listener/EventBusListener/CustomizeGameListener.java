package design.pattern.project2.listener.EventBusListener;

import design.pattern.project2.controller.MainController;

import javax.swing.*;

public class CustomizeGameListener extends GenericListener {
    private MainController mainController;

    public CustomizeGameListener(String componentName, MainController mainController) {
        super(componentName);
        this.mainController = mainController;
    }

    @Override
    public void update(Object item) {
        JTextField rowsField = new JTextField(5);
        JTextField columnsField = new JTextField(5);

        JPanel myPanel = new JPanel();
        myPanel.add(new JLabel("Rows:"));
        myPanel.add(rowsField);
        myPanel.add(Box.createHorizontalStrut(15)); // a spacer
        myPanel.add(new JLabel("Columns:"));
        myPanel.add(columnsField);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter X and Y Values", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            try{
                int rows = Integer.parseInt(rowsField.getText());
                int columns = Integer.parseInt(columnsField.getText());
                if (rows < 1 || columns < 1 || rows > 20 || columns > 20){
                    throw new Exception();
                }
                this.mainController.setUpGame(rows,columns);
            }catch (Exception ex){
                JOptionPane.showMessageDialog(null, "Wrong Input format! Please input a number more than 0 but less than 21!");
            }
        }
    }
}
