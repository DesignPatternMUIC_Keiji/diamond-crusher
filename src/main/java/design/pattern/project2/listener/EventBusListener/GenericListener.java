package design.pattern.project2.listener.EventBusListener;



public abstract class GenericListener {
    String componentName;

    public GenericListener(String componentName) {
        this.componentName = componentName;
    }


    public abstract void update(Object item);

    public String getComponentName() {
        return componentName;
    }
}
