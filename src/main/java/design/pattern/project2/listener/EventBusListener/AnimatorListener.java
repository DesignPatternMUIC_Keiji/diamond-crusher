package design.pattern.project2.listener.EventBusListener;

import design.pattern.project2.Worker.AnimationWorker;
import design.pattern.project2.gui.component.GamePanel;
import design.pattern.project2.model.Command;

import java.util.ArrayList;
import java.util.List;

public class AnimatorListener extends GenericListener {

    private GamePanel gamePanel;

    public AnimatorListener(String componentName, GamePanel gamePanel) {
        super(componentName);
        this.gamePanel = gamePanel;
    }

    @Override
    public void update(Object item) {
        List<Command> commandList = (ArrayList<Command>) item;
        AnimationWorker animationWorker = new AnimationWorker();
        animationWorker.setGamePanel(gamePanel);
        animationWorker.setCommands(commandList);
        animationWorker.execute();
    }
}
