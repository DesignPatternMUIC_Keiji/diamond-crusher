package design.pattern.project2.listener.EventBusListener;

import design.pattern.project2.controller.MainController;
import design.pattern.project2.gui.component.ScorePanel;

import javax.swing.*;

public class GameOverListener extends GenericListener{
    private MainController mainController;
    private ScorePanel scorePanel;

    public GameOverListener(String componentName, MainController mainController, ScorePanel scorePanel) {
        super(componentName);
        this.mainController = mainController;
        this.scorePanel = scorePanel;
    }

    @Override
    public void update(Object item) {
        JOptionPane.showMessageDialog(null, "No More Move!\nGame Over!\nFinal Score: " + scorePanel.getScore());
        mainController.newGame();
    }
}
