package design.pattern.project2.listener.EventBusListener;

import design.pattern.project2.gui.component.GamePanel;

public class StopAnimationListener extends GenericListener{
    private GamePanel gamePanel;

    public StopAnimationListener(String componentName, GamePanel gamePanel) {
        super(componentName);
        this.gamePanel = gamePanel;
    }

    @Override
    public void update(Object item) {
        this.gamePanel.setAnimating(false);
    }
}
