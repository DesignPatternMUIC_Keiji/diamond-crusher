package design.pattern.project2.listener.NotifyEventBusListener;

import design.pattern.project2.constant.ComponentName;
import design.pattern.project2.singleton.EventBus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewGameButtonListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        EventBus.notify(ComponentName.NEWGAME,null);
    }
}
