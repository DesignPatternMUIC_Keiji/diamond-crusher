package design.pattern.project2.listener.AnimationListener;

import design.pattern.project2.gui.component.GameLabel;
import design.pattern.project2.gui.component.GamePanel;
import design.pattern.project2.model.ClickCounter;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class HoverListener implements MouseListener{
    private ClickCounter clickCounter;
    private GamePanel gamePanel;
    private GameLabel gameLabel;

    public HoverListener(ClickCounter clickCounter, GamePanel gamePanel, GameLabel gameLabel){
        this.clickCounter = clickCounter;
        this.gamePanel = gamePanel;
        this.gameLabel = gameLabel;
    }

    /**
     * Click on item, and then do necessary change, increment counter, making the panel show that it's correct
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (!gamePanel.isAnimating()){
            this.gameLabel.setClicked();
            if (this.gameLabel.isClicked()){
                this.gamePanel.addPositionClicked(this.gameLabel.getPosition());
                this.clickCounter.increment();
            }else{
                this.gamePanel.removePositionClicked(this.gameLabel.getPosition());
                this.clickCounter.decrement();
            }
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        this.gameLabel.setHovered();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.gameLabel.setNotHovered();
    }
}