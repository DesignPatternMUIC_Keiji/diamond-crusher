package design.pattern.project2.listener.AnimationListener;

import design.pattern.project2.gui.component.GameLabel;
import design.pattern.project2.gui.component.GamePanel;
import design.pattern.project2.service.AnimationCalculationService;
import design.pattern.project2.service.impl.AnimationCalculationServiceImpl;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.concurrent.CountDownLatch;

public class SwapAnimationListener implements ActionListener{

    private AnimationCalculationService animationCalculationService = new AnimationCalculationServiceImpl();

    private GameLabel gameLabel1;
    private GameLabel gameLabel2;
    private GamePanel gamePanel;

    private LinkedList<Integer[]> path1;
    private LinkedList<Integer[]> path2;

    private CountDownLatch countDownLatch;

    private Timer timer;

    public void setGameLabel1(GameLabel gameLabel1) {
        this.gameLabel1 = gameLabel1;
    }

    public void setGameLabel2(GameLabel gameLabel2) {
        this.gameLabel2 = gameLabel2;
    }

    public void setPath1(LinkedList<Integer[]> path1) {
        this.path1 = path1;
    }

    public void setPath2(LinkedList<Integer[]> path2) {
        this.path2 = path2;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public void setGamePanel(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }

    public void setCountDownLatch(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        animationCalculationService.move(gameLabel1,path1.pop());
        animationCalculationService.move(gameLabel2,path2.pop());
        gamePanel.repaint();
        if (path1.isEmpty()){
            timer.stop();
            gamePanel.swap(gameLabel1.getPosition(),gameLabel2.getPosition());
            countDownLatch.countDown();
        }
    }
}
