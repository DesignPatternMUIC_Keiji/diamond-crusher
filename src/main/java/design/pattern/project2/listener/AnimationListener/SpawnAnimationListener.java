package design.pattern.project2.listener.AnimationListener;

import design.pattern.project2.gui.component.GameLabel;
import design.pattern.project2.gui.component.GamePanel;
import design.pattern.project2.service.AnimationCalculationService;
import design.pattern.project2.service.impl.AnimationCalculationServiceImpl;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class SpawnAnimationListener implements ActionListener {
    AnimationCalculationService animationCalculationService = new AnimationCalculationServiceImpl();
    GamePanel gamePanel;
    List<GameLabel> gameLabels;
    List<Integer[]> initials;
    List<Integer[]> destinations;
    List<LinkedList<Integer[]>> sizes;
    List<LinkedList<Integer[]>> boundXY;


    Timer timer;

    CountDownLatch countDownLatch;

    public void setGamePanel(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }

    public void setGameLabels(List<GameLabel> gameLabels) {
        this.gameLabels = gameLabels;
    }

    public void setInitials(List<Integer[]> initials) {
        this.initials = initials;
    }

    public void setDestinations(List<Integer[]> destinations) {
        this.destinations = destinations;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public void setCountDownLatch(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    public void setSizes(List<LinkedList<Integer[]>> sizes) {
        this.sizes = sizes;
    }

    public void setBoundXY(List<LinkedList<Integer[]>> boundXY) {
        this.boundXY = boundXY;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ((!sizes.isEmpty() && sizes.get(0).isEmpty()) || (!boundXY.isEmpty() && boundXY.get(0).isEmpty())){
            timer.stop();
            countDownLatch.countDown();
        }
        int recentS = -1;
        for (int i = 0; i < gameLabels.size(); i++){
            gameLabels.get(i).setBounds(boundXY.get(i).pop(),sizes.get(i).pop());
            recentS = boundXY.get(i).size();
        }
        if (recentS == 0){
            timer.stop();
            countDownLatch.countDown();
        }
        gamePanel.doLayout();
        gamePanel.repaint();

    }
}