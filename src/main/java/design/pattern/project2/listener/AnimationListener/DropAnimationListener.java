package design.pattern.project2.listener.AnimationListener;

import design.pattern.project2.gui.component.GameLabel;
import design.pattern.project2.gui.component.GamePanel;
import design.pattern.project2.service.AnimationCalculationService;
import design.pattern.project2.service.impl.AnimationCalculationServiceImpl;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class DropAnimationListener implements ActionListener {

    AnimationCalculationService animationCalculationService = new AnimationCalculationServiceImpl();
    GamePanel gamePanel;
    List<GameLabel> gameLabels;
    List<Integer[]> initials;
    List<Integer[]> destinations;
    List<LinkedList<Integer[]>> paths;

    Timer timer;

    CountDownLatch countDownLatch;

    public void setCountDownLatch(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    public void setGamePanel(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }

    public void setGameLabels(List<GameLabel> gameLabels) {
        this.gameLabels = gameLabels;
    }

    public void setInitials(List<Integer[]> initials) {
        this.initials = initials;
    }

    public void setDestinations(List<Integer[]> destinations) {
        this.destinations = destinations;
    }

    public void setPaths(List<LinkedList<Integer[]>> paths) {
        this.paths = paths;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < gameLabels.size(); i++){
            animationCalculationService.move(gameLabels.get(i), paths.get(i).pop());
        }
        gamePanel.repaint();
        if (!paths.isEmpty() && paths.get(0).isEmpty()){
            timer.stop();
            for (int i = 0; i < gameLabels.size();i++){
                gamePanel.swap(initials.get(i),destinations.get(i));
            }
            countDownLatch.countDown();
        }
    }
}