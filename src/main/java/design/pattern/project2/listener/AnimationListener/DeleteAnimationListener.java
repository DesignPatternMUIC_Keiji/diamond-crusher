package design.pattern.project2.listener.AnimationListener;

import design.pattern.project2.gui.component.GameLabel;
import design.pattern.project2.gui.component.GamePanel;
import design.pattern.project2.service.AnimationCalculationService;
import design.pattern.project2.service.impl.AnimationCalculationServiceImpl;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.concurrent.CountDownLatch;

public class DeleteAnimationListener  implements ActionListener{

    AnimationCalculationService animationCalculationService = new AnimationCalculationServiceImpl();

    GameLabel gameLabel1;
    GamePanel gamePanel;

    LinkedList<Integer[]> boundsXY;
    LinkedList<Integer[]> size;


    Timer timer;

    CountDownLatch countDownLatch;

    public void setCountDownLatch(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    public void setGameLabel1(GameLabel gameLabel1) {
        this.gameLabel1 = gameLabel1;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public void setBoundsXY(LinkedList<Integer[]> boundsXY) {
        this.boundsXY = boundsXY;
    }

    public void setSize(LinkedList<Integer[]> size) {
        this.size = size;
    }

    public void setGamePanel(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        gameLabel1.setBounds(boundsXY.pop(),size.pop());

        if (gameLabel1.getBounds().getWidth() <= 0) {
            timer.stop();
            gamePanel.destroyAPosition(gameLabel1.getPosition());
            countDownLatch.countDown();
        }
    }
}
