package design.pattern.project2.Worker;

import design.pattern.project2.constant.ComponentName;
import design.pattern.project2.constant.Constant;
import design.pattern.project2.gui.component.GamePanel;
import design.pattern.project2.model.Block;
import design.pattern.project2.model.Board;
import design.pattern.project2.model.Command;
import design.pattern.project2.service.AnimationCalculationService;
import design.pattern.project2.service.impl.AnimationCalculationServiceImpl;
import design.pattern.project2.singleton.EventBus;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class AnimationWorker extends SwingWorker<Board, Board> {

    private AnimationCalculationService animationCalculationService = new AnimationCalculationServiceImpl();


    private GamePanel gamePanel;


    private List<Command> commands;




    public AnimationWorker() { }

    public AnimationWorker(GamePanel gamePanel, List<Command> commands) {
        this.gamePanel = gamePanel;
        this.commands = commands;
    }

    public void setGamePanel(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }


    public void setCommands(List<Command> commands) {
        this.commands = commands;
    }


    //SWITCH -> DELETE -> MOVE -> SPAWN
    @Override
    protected Board doInBackground() throws Exception {
        List<Integer[]> positions = this.gamePanel.getPositionsClicked();
        this.gamePanel.resetClicks();
        this.gamePanel.setAnimating(true);
        CountDownLatch countDownLatch = new CountDownLatch(0);
        for (Command command: commands){
            System.out.println(command.getAction());
            if (command.getAction() == Constant.ACTION_SWITCH){
                countDownLatch = new CountDownLatch(1);
                animationCalculationService.swapPositions(gamePanel, countDownLatch,positions.get(0),positions.get(1));
            }else if(command.getAction() == Constant.ACTION_DELETE){
                List<Integer[]>  toBeDeleted = findDeletedPosition(command.getBoard());
                EventBus.notify(ComponentName.ADDSCORE, toBeDeleted.size());
                countDownLatch = new CountDownLatch(toBeDeleted.size());
                for (Integer[] item: toBeDeleted){
                    animationCalculationService.deleteAPosition(gamePanel,countDownLatch,item);
                }
            }else if(command.getAction() == Constant.ACTION_MOVE){
                Map<Integer, List<Block>> mapBlock = command.getBoard().getBlockMap();
                countDownLatch = new CountDownLatch(mapBlock.keySet().size());
                for (int c = 0; c < command.getBoard().getNoOfColumn(); c++){
                    if (mapBlock.containsKey(c)){
                        for (int i = 0; i < mapBlock.get(c).size(); i++){
                            animationCalculationService.moveBlockDown(gamePanel,countDownLatch,mapBlock.get(c).get(i));
                        }
                    }
                }
            }else if(command.getAction() == Constant.ACTION_SPAWN){
                Map<Integer, List<Block>> mapBlock = command.getBoard().getBlockMap();
                countDownLatch = new CountDownLatch(mapBlock.keySet().size());
                //spawn everything first
                for (int i = 0; i < command.getBoard().getNoOfRow(); i++){
                    for (int j = 0; j < command.getBoard().getNoOfColumn(); j++){
                        if (gamePanel.getGameLabelInThatPosition(i,j) == null){
                            gamePanel.spawnGameLabel(i,j);
                            gamePanel.getGameLabelInThatPosition(i,j).setValue(command.getBoard().getBoardStructure()[i][j].getType());
                            gamePanel.add(gamePanel.getGameLabelInThatPosition(i,j));
                        }
                    }
                }
                for (int c = 0; c < command.getBoard().getNoOfColumn(); c++){
                    if (mapBlock.containsKey(c)){
                        for (int i = 0; i < mapBlock.get(c).size(); i++){
                            animationCalculationService.spawnBlock(gamePanel,countDownLatch,mapBlock.get(c).get(i));
                        }
                    }
                }
            }
            try{
                countDownLatch.await();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }

        EventBus.notify(ComponentName.STOP,null);

        if (commands.get(commands.size()-1).getBoard().isGameOver()){
            EventBus.notify(ComponentName.GAMEOVER, null);
        }
        return commands.get(commands.size()-1).getBoard();
    }


    public static void printBoard(Board board){
        for (int i = 0; i < board.getNoOfRow(); i++){
            for (int j = 0; j < board.getNoOfColumn(); j++){
                if (board.getBoardStructure()[i][j] == null){
                    System.out.print("0 ");
                }else{
                    System.out.print(board.getBoardStructure()[i][j].getType() + " ");
                }
            }
            System.out.println();
        }
    }

    private List<Integer[]> findDeletedPosition(Board board){
        List<Integer[]> items=  new ArrayList<>();
        for (int i = 0; i < board.getNoOfRow(); i++){
            for (int j = 0; j < board.getNoOfColumn(); j++){
                if (board.getBoardStructure()[i][j] == null){
                    Integer[] item = new Integer[2];
                    item[0] = i;
                    item[1] = j;
                    items.add(item);
                }
            }
        }
        return items;
    }

}
