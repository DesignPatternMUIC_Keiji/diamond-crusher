package design.pattern.project2.Worker;

import javax.swing.*;

public class TimeWorker extends SwingWorker<Object,Object> {

    JLabel timer;
    long start;
    long prev;
    long limit = 1000 * 60 * 5;
//    long limit = 1000 * 10;
    public TimeWorker(JLabel timer) {
        this.timer = timer;
        this.start = System.currentTimeMillis();
        this.prev = this.start;
    }

    /**
     * proper time is a string of min:seconds left
     * @param time
     * @return
     */
    private String convertToProperTime(long time){
        long minute = time/(60 * 1000);
        long seconds = time%(60 * 1000)/1000;
        return  minute + ":" + seconds;
    }

    @Override
    protected Object doInBackground() throws Exception {
        while (this.start + this.limit > System.currentTimeMillis()){
            if (System.currentTimeMillis() - this.prev > 100){
                timer.setText(convertToProperTime((this.start+this.limit) - System.currentTimeMillis()));
                this.prev = System.currentTimeMillis();
            }
        }
        setProgress(100);
        return null;
    }
}
