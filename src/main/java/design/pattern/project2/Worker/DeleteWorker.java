package design.pattern.project2.Worker;

import design.pattern.project2.gui.component.GameLabel;
import design.pattern.project2.gui.component.GamePanel;
import design.pattern.project2.service.AnimationCalculationService;
import design.pattern.project2.service.impl.AnimationCalculationServiceImpl;

import javax.swing.*;

public class DeleteWorker extends SwingWorker<Object,Object>{

    AnimationCalculationService animationCalculationService = new AnimationCalculationServiceImpl();
    GamePanel gamePanel;
    Integer[] pos;

    public DeleteWorker(GamePanel gamePanel, Integer[] pos) {
        this.gamePanel = gamePanel;
        this.pos = pos;
    }

    @Override
    protected Object doInBackground() throws Exception {
//        animationCalculationService.deleteAPosition(gamePanel,c,pos);
        return null;
    }
}
