package design.pattern.project2.service.impl;

import design.pattern.project2.constant.Constant;
import design.pattern.project2.model.Board;
import design.pattern.project2.model.Command;
import design.pattern.project2.service.BlockService;
import design.pattern.project2.service.BoardCheckService;
import design.pattern.project2.service.BoardService;
import design.pattern.project2.service.GameService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Trung on 10/8/2017.
 */
public class GameServiceImpl implements GameService {

    private final BoardService boardService = new BoardServiceImpl();

    private final BoardCheckService boardCheckService = new BoardCheckServiceImpl();

    private final BlockService blockService = new BlockServiceImpl();

    /**
     * Generate command list for frontend
     * @param board
     * @param row1
     * @param column1
     * @param row2
     * @param column2
     * @return
     */
    @Override
    public List<Command> switchNode(Board board, int row1, int column1, int row2, int column2) {
        List<Command> commandList = new ArrayList<>();

        // If switch is not valid
        if (!boardCheckService.isValidSwitch(row1, column1, row2, column2)) {
            commandList.add(new Command(board, Constant.ACTION_COMPLETE));
            return commandList;
        }

        // Generate action lists
        boardService.switchNode(board, row1, column1, row2, column2);
        commandList.add(new Command(board, Constant.ACTION_SWITCH));

        // Deal with bomb
        int[] bomb = boardCheckService.checkBombBySwitchedNodes(board, row1, column1, row2, column2);
        // If one of 2 switched node is bomb
        if (bomb[0] != Integer.MIN_VALUE) boardService.scanBoard(board, bomb[0], bomb[1]);

        // While can still delete
        while (boardService.canDelete(board))
            board = this.genericActions(commandList, board);

        // If swap is inconsequential, swap back
        if (commandList.size() == 1) {
            boardService.switchNode(board, row1, column1, row2, column2);
            commandList.add(new Command(board, Constant.ACTION_SWITCH));
        }

        Board finalBoard = boardService.cloneBoard(commandList.get(commandList.size()-1).getBoard());
        boolean isGameOver = boardService.checkGameOverStatus(finalBoard);
        finalBoard.setGameOver(isGameOver);
        // Add COMPLETE action to final position
        commandList.add(new Command(finalBoard, Constant.ACTION_COMPLETE));


        return commandList;
    }

    /**
     * Generic actions
     * @param board
     * @param commandList
     */
    private Board genericActions(List<Command> commandList, Board board) {
        Board board1 = boardService.cloneBoard(board);
        boardService.scanBoard(board1);
        boardService.deleteNode(board1);
        // Send command if action can be done
        if (!Arrays.deepEquals(board1.getBoardStructure(), board.getBoardStructure()))
            commandList.add(new Command(board1, Constant.ACTION_DELETE));

        Board board2 = boardService.cloneBoard(board1);
        blockService.generateBlock(board2, false);
        boardService.moveDown(board2);
        // Send command if action can be done
        if (!Arrays.deepEquals(board2.getBoardStructure(), board1.getBoardStructure()))
            commandList.add(new Command(board2, Constant.ACTION_MOVE));

        Board board3 = boardService.cloneBoard(board2);
        blockService.generateBlock(board3, true);
        boardService.spawnBoardNode(board3);
        // Send command if action can be done
        if (!Arrays.deepEquals(board3.getBoardStructure(), board2.getBoardStructure()))
            commandList.add(new Command(board3, Constant.ACTION_SPAWN));

        return board3;
    }
}
