package design.pattern.project2.service.impl;

import design.pattern.project2.constant.Constant;
import design.pattern.project2.model.Board;
import design.pattern.project2.model.Node;
import design.pattern.project2.service.BoardCheckService;
import design.pattern.project2.service.NodeMarkingService;

/**
 * Created by Trung on 10/5/2017.
 */
public class NodeMarkingServiceImpl implements NodeMarkingService {

    private final BoardCheckService boardCheckService = new BoardCheckServiceImpl();

    /**
     * Mark node
     * @param board
     * @param row
     * @param column
     */
    @Override
    public void markNode(Board board, int row, int column) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();
        Node[][] boardStructure = board.getBoardStructure();

        boolean horizontalCheck = boardCheckService.checkHorizontal(board, row, column);
        boolean verticalCheck = boardCheckService.checkVertical(board, row, column);

        // Mark horizontal nodes
        if (horizontalCheck) {
            // Check on left
            if (column >= 1 && boardStructure[row][column-1].getType() == boardStructure[row][column].getType())
                this.markLeft(board, row, column - 1);
            // Check on right
            if (column <= boardColumn-2
                    && boardStructure[row][column+1].getType() == boardStructure[row][column].getType())
                this.markRight(board, row, column+1);
        }

        // Mark vertical nodes
        if (verticalCheck) {
            // Check on top
            if (row >= 1 && boardStructure[row-1][column].getType() == boardStructure[row][column].getType())
                this.markTop(board, row, column);
            // Check on bottom
            if (row <= boardRow-2 && boardStructure[row+1][column] == boardStructure[row][column])
                this.markBottom(board, row, column);
        }

        // Mark first Node
        boardStructure[row][column].setAction(Constant.ACTION_DELETE);
    }

    /**
     * Mark nodes that are affected by bomb
     * @param board
     * @param row
     * @param column
     */
    public void markBombNode(Board board, int row, int column) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();
        Node[][] boardStructure = board.getBoardStructure();

        for (int i = 0; i < boardRow; i++)
            boardStructure[i][column].setAction(Constant.ACTION_DELETE);

        for (int i = 0; i < boardColumn; i++)
            boardStructure[row][i].setAction(Constant.ACTION_DELETE);
    }

    /**
     * Mark node recursively to left
     * @param board
     * @param row
     * @param column
     */
    private void markLeft(Board board, int row, int column) {
        Node[][] boardStructure = board.getBoardStructure();

        if (column == 0 || boardStructure[row][column-1].getType() != boardStructure[row][column].getType()) {
            boardStructure[row][column].setAction(Constant.ACTION_DELETE);
            return;
        } else {
            boardStructure[row][column].setAction(Constant.ACTION_DELETE);
            markLeft(board, row, column-1);
        }
    }

    /**
     * Mark node recursively to right
     * @param board
     * @param row
     * @param column
     */
    private void markRight(Board board, int row, int column) {
        int boardColumn = board.getNoOfColumn();
        Node[][] boardStructure = board.getBoardStructure();

        if (column == boardColumn-1 || boardStructure[row][column+1].getType() != boardStructure[row][column].getType()) {
            boardStructure[row][column].setAction(Constant.ACTION_DELETE);
            return;
        } else {
            boardStructure[row][column].setAction(Constant.ACTION_DELETE);
            markRight(board, row, column+1);
        }
    }

    /**
     * Mark node recursively to top
     * @param board
     * @param row
     * @param column
     */
    public void markTop(Board board, int row, int column) {
        Node[][] boardStructure = board.getBoardStructure();

        if (row == 0 || boardStructure[row-1][column].getType() != boardStructure[row][column].getType()) {
            boardStructure[row][column].setAction(Constant.ACTION_DELETE);
            return;
        } else {
            boardStructure[row][column].setAction(Constant.ACTION_DELETE);
            markTop(board, row-1, column);
        }
    }

    /**
     * Mark node recursively to bottom
     * @param board
     * @param row
     * @param column
     */
    private void markBottom(Board board, int row, int column) {
        int boardRow = board.getNoOfRow();
        Node[][] boardStructure = board.getBoardStructure();

        if (row == boardRow-1 || boardStructure[row+1][column].getType() != boardStructure[row][column].getType()) {
            boardStructure[row][column].setAction(Constant.ACTION_DELETE);
            return;
        } else {
            boardStructure[row][column].setAction(Constant.ACTION_DELETE);
            markBottom(board, row+1, column);
        }
    }
}
