package design.pattern.project2.service.impl;

import design.pattern.project2.constant.View;
import design.pattern.project2.service.DimensionCalculationService;

public class DimensionCalculationServiceImpl implements DimensionCalculationService {

    /**
     * [0] is width, [1] is height
     * x/columns=width, y/rows=height
     * require, how many grid in the board
     * We need more offset for some stuff later but right now we keep it at 0
     * @return array int of width and height respectively [width,height]
     */
    public int[] calculateMainFrameDimensions(int rows, int columns){
        int columnOffset = calculateScorePanelDimensions()[0] + calculateTimerDimensions()[0];
        int rowOffset = calculateMenuBarDimensions(columns)[1];
        int[] dimensions = new int[2];
        dimensions[0] = columns * View.COLUMN_SIZE + columnOffset;
        dimensions[1] = rows * View.ROW_SIZE + rowOffset;
        return dimensions;
    }

    public int[] calculateGamePanelDimensions(int rows, int columns){
        int[] dimensions = new int[2];
        dimensions[0] = columns * View.COLUMN_SIZE;
        dimensions[1] = rows * View.ROW_SIZE;
        return dimensions;
    }

    public int[] calculateMenuBarDimensions(int columns) {
        int[] dimensions = new int[2];
        dimensions[0] = columns * View.COLUMN_SIZE;
        dimensions[1] = 60;
        return dimensions;
    }

    public int[] calculateScorePanelDimensions(){
        int[] dimensions = new int[2];
        dimensions[0] = 60;
        dimensions[1] = 300;
        return  dimensions;
    }

    @Override
    public int[] calculateTimerDimensions() {
        int[] dimensions = new int[2];
        dimensions[0] = 60;
        dimensions[1] = 300;
        return dimensions;
    }

    public int[] calculateOffSet(int rowNum, int colNum){
        int[] pos = new int[2];
        pos[0] = colNum*View.COLUMN_SIZE;
        pos[1] = rowNum*View.ROW_SIZE;
        return pos;
    }
}
