package design.pattern.project2.service.impl;

import design.pattern.project2.model.Board;
import design.pattern.project2.model.Node;
import design.pattern.project2.service.BoardCheckService;

/**
 * Created by Trung on 10/5/2017.
 */
public class BoardCheckServiceImpl implements BoardCheckService{

    /**
     * Implement checkHorizontal in BoardService
     * @param board
     * @param row is specific given row, not max row
     * @param column similarly
     * @return
     */
    @Override
    public boolean checkHorizontal(Board board, int row, int column) {
        int boardColumn = board.getNoOfColumn();
        Node[][] boardStructure = board.getBoardStructure();

        // 2 to the left
        if (column >= 2 && boardStructure[row][column-1].getType() == boardStructure[row][column].getType()
                        && boardStructure[row][column-2].getType() == boardStructure[row][column].getType())
            return true;

        // 2 to the right
        else if (column <= boardColumn-3
                        && boardStructure[row][column+1].getType() == boardStructure[row][column].getType()
                        && boardStructure[row][column+2].getType() == boardStructure[row][column].getType())
            return true;

        // 1 left 1 right
        else if (column >= 1 && column <= boardColumn - 2
                        && boardStructure[row][column+1].getType() == boardStructure[row][column].getType()
                        && boardStructure[row][column-1].getType() == boardStructure[row][column].getType())
            return true;
        else
            return false;
    }

    /**
     * Implement checkVertial in BoardService
     * @param board
     * @param row is specific given row, not max row
     * @param column similarly
     * @return
     */
    @Override
    public boolean checkVertical(Board board, int row, int column) {
        int boardRow = board.getNoOfRow();
        Node[][] boardStructure = board.getBoardStructure();

        // 2 on top
        if (row >= 2 && boardStructure[row-1][column].getType() == boardStructure[row][column].getType()
                    && boardStructure[row-2][column].getType() == boardStructure[row][column].getType())
            return true;

        // 2 at bottom
        else if (row <= boardRow-3
                    && boardStructure[row+1][column].getType() == boardStructure[row][column].getType()
                    && boardStructure[row+2][column].getType() == boardStructure[row][column].getType())
            return true;

        // 1 top 1 bottom
        else if (row >= 1 && row <= boardRow - 2
                    && boardStructure[row+1][column].getType() == boardStructure[row][column].getType()
                    && boardStructure[row-1][column].getType() == boardStructure[row][column].getType())
            return true;
        else
            return false;
    }

    /**
     * Check of switch is valid
     * @param row1
     * @param column1
     * @param row2
     * @param column2
     * @return
     */
    @Override
    public boolean isValidSwitch(int row1, int column1, int row2, int column2) {
        // Same column
        if (column1 == column2)
            return (row1-1 == row2 || row1+1 == row2);
        // Same row
        if (row1 == row2)
            return (column1-1 == column2 || column1 + 1 == column2);
        return false;
    }

    /**
     * Check if switched nodes are bombs
     * @param board
     * @param row1
     * @param column1
     * @param row2
     * @param column2
     * @return
     */
    @Override
    public int[] checkBombBySwitchedNodes(Board board, int row1, int column1, int row2, int column2) {
        Node[][] boardStructure = board.getBoardStructure();

        if (boardStructure[row1][column1].getType() == 7) return new int[]{row1, column1};
        else if (boardStructure[row2][column2].getType() == 7) return new int[]{row2, column2};

        return new int[]{Integer.MIN_VALUE, Integer.MIN_VALUE};
    }

    /**
     * Check if board contains a bomb
     * @param board
     * @return
     */
    @Override
    public boolean isBoardContainsBomb(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();
        Node[][] boardStructure = board.getBoardStructure();

        for (int i = 0; i < boardRow; i++) {
            for (int j = 0; j < boardColumn; j++)
                if (boardStructure[i][j] != null)
                    if (boardStructure[i][j].getType() == 7) return true;
        }
        return false;
    }
}
