package design.pattern.project2.service.impl;

import design.pattern.project2.constant.View;
import design.pattern.project2.gui.component.GameLabel;
import design.pattern.project2.gui.component.GamePanel;
import design.pattern.project2.listener.AnimationListener.DeleteAnimationListener;
import design.pattern.project2.listener.AnimationListener.DropAnimationListener;
import design.pattern.project2.listener.AnimationListener.SpawnAnimationListener;
import design.pattern.project2.listener.AnimationListener.SwapAnimationListener;
import design.pattern.project2.model.Block;
import design.pattern.project2.service.AnimationCalculationService;
import design.pattern.project2.service.DimensionCalculationService;

import javax.swing.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class AnimationCalculationServiceImpl implements AnimationCalculationService {

    private float frames = 60;
    private int delay = 5;

    DimensionCalculationService dimensionCalculationService = new DimensionCalculationServiceImpl();

    /**
     * Deleting a position, given a position, call the game panel and set that location
     * To be smaller?? Or make a line boarder to be bigger bigger bigger until it's small
     * Then to destroy that game label by setting that location of the board to be null
     * Each Iteration of growing boarder, there must be a swing pause
     * Integer[] n is the position of the node in term of row and column, not x and y
     */
    @Override
    public void deleteAPosition(GamePanel gamePanel, CountDownLatch countDownLatch, Integer[] n) {
        GameLabel gameLabel = gamePanel.getGameLabelInThatPosition(n);

        int[] bounds = new int[2];
        bounds[0] = (int) gameLabel.getBounds().getX();
        bounds[1] = (int) gameLabel.getBounds().getY();
        int[] destBounds = new int[2];
        destBounds[0] = bounds[0] + View.COLUMN_SIZE/2;
        destBounds[1] = bounds[1] + View.ROW_SIZE/2;

        int[] size = new int[2];
        size[0] = (int) gameLabel.getBounds().getWidth();
        size[1] = (int) gameLabel.getBounds().getHeight();

        DeleteAnimationListener deleteAnimationListener = new DeleteAnimationListener();
        deleteAnimationListener.setGameLabel1(gameLabel);
        deleteAnimationListener.setBoundsXY(createPath(bounds,destBounds));
        deleteAnimationListener.setSize(makeSizeForMakingAPositionSmaller(size));
        deleteAnimationListener.setGamePanel(gamePanel);
        deleteAnimationListener.setCountDownLatch(countDownLatch);
        Timer timer = new Timer(delay,deleteAnimationListener);
        deleteAnimationListener.setTimer(timer);
        timer.setDelay(delay);
        timer.setRepeats(true);
        timer.start();
    }


    /**
     * this shit should be easy, but it's not so i dont know wtf to do
     * swapping 2 nodes, how the fuck should i do it
     * Integer[] n is the position of the node in term of row and column, not x and y
     * 1) Take the 2 gamelabels from their locations (n1,n2)
     * 2) take turn swapping the 2 nodes
     *
     * @param gamePanel
     * @param n1
     * @param n2
     */
    @Override
    public void swapPositions(GamePanel gamePanel, CountDownLatch countDownLatch, Integer[] n1, Integer[] n2) {
        GameLabel node1 = gamePanel.getGameLabelInThatPosition(n1);
        GameLabel node2 = gamePanel.getGameLabelInThatPosition(n2);
        LinkedList<Integer[]> path1 = createPath(dimensionCalculationService.calculateOffSet(n1[0],n1[1]),
                                                dimensionCalculationService.calculateOffSet(n2[0],n2[1]));
        LinkedList<Integer[]> path2 = createPath(dimensionCalculationService.calculateOffSet(n2[0],n2[1]),
                                                 dimensionCalculationService.calculateOffSet(n1[0],n1[1]));
        SwapAnimationListener taskPerformer = new SwapAnimationListener();

        taskPerformer.setGameLabel1(node1);
        taskPerformer.setGameLabel2(node2);
        taskPerformer.setPath1(path1);
        taskPerformer.setPath2(path2);
        taskPerformer.setGamePanel(gamePanel);
        taskPerformer.setCountDownLatch(countDownLatch);

        Timer timer = new Timer(delay,taskPerformer);

        taskPerformer.setTimer(timer);

        timer.setDelay(delay);
        timer.setRepeats(true);
        timer.start();
    }

    /**
     * Given move it down according to the block property
     * block has its own drop distance
     * 1) get all the gameLabels of the block
     * 2) Calculate their final positions (destinations) into List<LinkedList<Integer[]>>
     * 3) Create a DropAnimationListener
     * 4) Set the animation into motion
     */
    @Override
    public void moveBlockDown(GamePanel gamePanel, CountDownLatch countDownLatch, Block block) {
        List<GameLabel> gameLabels = new ArrayList<>();
        List<LinkedList<Integer[]>> paths = new ArrayList<>();
        List<Integer[]> initials = new ArrayList<>();
        List<Integer[]> destinations = new ArrayList<>();

        for (int i = 0; i < block.getBlock().size(); i ++){
            Integer[] initialPos = getInitialPosition(block.getBlock().get(i),block.getDropDistance());
            Integer[] destPos = new Integer[2];
            destPos[0] = block.getBlock().get(i)[0];
            destPos[1] = block.getBlock().get(i)[1];
            GameLabel gameLabel = gamePanel.getGameLabelInThatPosition(initialPos);
            gameLabels.add(gameLabel);
            initials.add(initialPos);
            destinations.add(destPos);
            paths.add(createPath(dimensionCalculationService.calculateOffSet(initialPos[0],initialPos[1])
                                ,dimensionCalculationService.calculateOffSet(destPos[0],destPos[1])));
        }
        DropAnimationListener dropAnimationListener = new DropAnimationListener();
        Timer timer = new Timer(delay,dropAnimationListener);
        dropAnimationListener.setGamePanel(gamePanel);
        dropAnimationListener.setGameLabels(gameLabels);
        dropAnimationListener.setInitials(initials);
        dropAnimationListener.setDestinations(destinations);
        dropAnimationListener.setPaths(paths);
        dropAnimationListener.setTimer(timer);
        dropAnimationListener.setCountDownLatch(countDownLatch);

        timer.setRepeats(true);
        timer.setDelay(delay);
        timer.start();
    }

    @Override
    public void spawnBlock(GamePanel gamePanel, CountDownLatch countDownLatch, Block block) {
        List<GameLabel> gameLabels = new ArrayList<>();
        List<LinkedList<Integer[]>> sizes = new ArrayList<>();
        List<LinkedList<Integer[]>> boundXY = new ArrayList<>();
        List<Integer[]> destinations = new ArrayList<>();

        for (int i = 0; i < block.getBlock().size(); i ++){
            Integer[] destPos = new Integer[2];
            destPos[0] = block.getBlock().get(i)[0];
            destPos[1] = block.getBlock().get(i)[1];
            GameLabel gameLabel = gamePanel.getGameLabelInThatPosition(destPos);
            int[] destOffset = dimensionCalculationService.calculateOffSet(destPos[0],destPos[1]);
            int[] iniOffset = new int[2];
            iniOffset[0] = destOffset[0] + View.COLUMN_SIZE/2;
            iniOffset[1] = destOffset[1] + View.ROW_SIZE/2;
            gameLabels.add(gameLabel);
            boundXY.add(createPath(iniOffset,destOffset));
            destinations.add(destPos);
            sizes.add(makeSizeForSpawn());
        }

        SpawnAnimationListener spawnAnimationListener = new SpawnAnimationListener();
        Timer timer = new Timer(delay,spawnAnimationListener);
        spawnAnimationListener.setGamePanel(gamePanel);
        spawnAnimationListener.setGameLabels(gameLabels);
        spawnAnimationListener.setDestinations(destinations);
        spawnAnimationListener.setSizes(sizes);
        spawnAnimationListener.setBoundXY(boundXY);
        spawnAnimationListener.setTimer(timer);
        spawnAnimationListener.setCountDownLatch(countDownLatch);

        timer.setRepeats(true);
        timer.setDelay(delay);
        timer.start();
    }


    /**
     * given 2 locations: initial and destination
     * make a List of locations for that gameLabel to move to
     * What is Location?
     * it is the offset of the gameLabel in term of x and y
     * Therefore, we are actually making a new offset for both x and y
     * @param initial
     * @param destination
     * @return
     */
    private LinkedList<Integer[]> createPath(int[] initial, int[] destination){
        return createPath(initial[0],initial[1],destination[0],destination[1]);
    }
    private LinkedList<Integer[]> createPath(Integer[] initial, Integer[] destination){
        return createPath(initial[0],initial[1],destination[0],destination[1]);
    }

    private LinkedList<Integer[]> createPath(int x1, int y1, int x2, int y2){
        LinkedList<Integer[]> path = new LinkedList<>();
        int differenceInX = x2 - x1;
        int differenceInY = y2 - y1;
        for (int i = 0; i <= (int) frames; i++){
            Integer[] newLoc = new Integer[2];
            newLoc[0] = x1 +
                    (int)(differenceInX/frames * i);
            newLoc[1] = y1 +
                    (int)(differenceInY/frames * i);
            path.add(newLoc);
        }
        return path;
    }


    private LinkedList<Integer[]> makeSizeForSpawn(){
        LinkedList<Integer[]> path = new LinkedList<>();
        for (int i = 0; i <= frames; i++){
            Integer[] newLoc = new Integer[2];
            newLoc[0] = (int) (View.COLUMN_SIZE/frames * i);
            newLoc[1] = (int) (View.ROW_SIZE/frames * i);
            path.add(newLoc);
        }
        return path;
    }

    private LinkedList<Integer[]> makeSizeForMakingAPositionSmaller(int[] initial){
        LinkedList<Integer[]> path = new LinkedList<>();
        for (int i = (int) frames; i >= 0; i--){
            Integer[] newLoc = new Integer[2];
            newLoc[0] = (int) (initial[0]/frames * i);
            newLoc[1] = (int) (initial[1]/frames * i);
            path.add(newLoc);
        }
        return path;
    }

    /**
     * set new bound
     * @param gameLabel
     * @param position
     */
    public void move(GameLabel gameLabel, Integer[] position){
        gameLabel.setBounds(position[0],position[1], View.COLUMN_SIZE, View.ROW_SIZE);
        gameLabel.repaint();
    }

    private Integer[] getInitialPosition(int[] dest, int drop){
        Integer[] ini = new Integer[2];
        ini[0] = dest[0]-drop;
        ini[1] = dest[1];
        return ini;
    }

    private Integer[] getInitialPosition(Integer[] dest, int drop){
        Integer[] ini = new Integer[2];
        ini[0] = dest[0]-drop;
        ini[1] = dest[1];
        return ini;
    }
}
