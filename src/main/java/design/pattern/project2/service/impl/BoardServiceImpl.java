package design.pattern.project2.service.impl;

import design.pattern.project2.constant.Constant;
import design.pattern.project2.model.Board;
import design.pattern.project2.model.Node;
import design.pattern.project2.service.BoardCheckService;
import design.pattern.project2.service.BoardService;
import design.pattern.project2.service.NodeMarkingService;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Trung on 10/1/2017.
 */
public class BoardServiceImpl implements BoardService {

    private final BoardCheckService boardCheckService = new BoardCheckServiceImpl();

    private final NodeMarkingService nodeMarkingService = new NodeMarkingServiceImpl();

    /**
     * Initialize board
     * @param board
     */
    @Override
    public void setBoard(Board board) {
        board.setGameOver(false);
        this.fillBoardByCondition(board, false);
    }

    /**
     * Spawn board nodes after deletion
     * @param board
     */
    @Override
    public int spawnBoardNode(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();
        Node[][] boardStructure = board.getBoardStructure();

        int spawnCounter = 0;

        for (int i = 0; i < boardRow; i++) {
            for (int j = 0; j < boardColumn; j++) {
                if (boardStructure[i][j] == null) {
                    int type;
                    // Only 1 bomb in board at any time
                    if (boardCheckService.isBoardContainsBomb(board))
                        type = ThreadLocalRandom.current().nextInt(1, 7);
                    else {
                        type = ThreadLocalRandom.current().nextInt(1, 8);
                        // So bombs don't appear to often
                        if (type == 7) type = ThreadLocalRandom.current().nextInt(1, 8);
                    }

                    boardStructure[i][j] = new Node(type, Constant.ACTION_SPAWN);
                    spawnCounter++;
                }
            }
        }
        return spawnCounter;
    }

    /**
     * Swap node
     * @param board
     * @param row1
     * @param column1
     * @param row2
     * @param column2
     */
    @Override
    public void switchNode(Board board, int row1, int column1, int row2, int column2) {
        Node[][] boardStructure = board.getBoardStructure();

        Node copy = boardStructure[row1][column1];
        copy.setAction(Constant.ACTION_SWITCH);

        boardStructure[row1][column1] = boardStructure[row2][column2];
        boardStructure[row1][column1].setAction(Constant.ACTION_SWITCH);

        boardStructure[row2][column2] = copy;
    }

    /**
     * TODO: Need action move
     * Move board after calculation
     * @param board
     */
    @Override
    public void moveDown(Board board) {
        List<List<Node>> allNodes = this.getAllNodes(board);

        // Fill board with null
        this.fillBoardByCondition(board, true);
        this.moveBoard(board, allNodes);
    }

    /**
     * Scan board for 3 or more nodes with same type
     * @param board
     */
    @Override
    public void scanBoard(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();
        Node[][] boardStructure = board.getBoardStructure();

        boolean scanningOver = true;
        for (int i = 0; i < boardRow; i++) {
            for (int j = 0; j < boardColumn; j++) {
                boolean horizontalCheck = boardCheckService.checkHorizontal(board, i, j);
                boolean verticalCheck = boardCheckService.checkVertical(board, i, j);

                if (boardStructure[i][j] != null && boardStructure[i][j].getAction() != Constant.ACTION_DELETE
                        && (horizontalCheck || verticalCheck)) {
                    nodeMarkingService.markNode(board, i, j);
                    scanningOver = false;
                }
            }
        }

        if (scanningOver) return;
        else scanBoard(board);
    }

    /**
     * Scan board for deletable nodes by bomb
     * @param board
     */
    @Override
    public void scanBoard(Board board, int row, int column) {
        nodeMarkingService.markBombNode(board, row, column);
    }

    /**
     * Delete marked nodes
     * @param board
     */
    @Override
    public void deleteNode(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();
        Node[][] boardStructure = board.getBoardStructure();

        for (int i = 0; i < boardRow; i++) {
            for (int j = 0; j < boardColumn; j++) {
                Node currentNode = boardStructure[i][j];
                if (currentNode.getAction() != null && currentNode.getAction() == Constant.ACTION_DELETE)
                    boardStructure[i][j] = null;
            }
        }
    }

    /**
     * TODO: Finish this function
     * check game over
     * @param board
     * @return
     */
    @Override
    public boolean checkGameOverStatus(Board board) {
        return (!boardCheckService.isBoardContainsBomb(board) &&
                this.isGameOverHorizontal(board) && this.isGameOverVertical(board));
    }

    /**
     * translate board
     * basically, each attribute that has meaning and should be told to the player will have their own mark
     * say: color -> 100, 200, 300
     * say: bomb -> 10, 20
     * say: other attribute -> 1, 2, 3
     * @param board
     * @return
     */
    @Override
    public int[][] translateBoard(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();
        Node[][] boardStructure = board.getBoardStructure();

        int[][] translatedBoard = new int[boardRow][boardColumn];

        for (int i = 0; i < boardRow; i++){
            for (int j = 0; j < boardColumn; j++){
                translatedBoard[i][j] = boardStructure[i][j].getType();
            }
        }

        return translatedBoard;
    }

    /**
     * Clone board
     * @param board
     * @return
     */
    @Override
    public Board cloneBoard(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();

        Board cloneBoard = new Board(boardRow, boardColumn);
        this.fillBoardByCondition(cloneBoard, true);

        Node[][] originalBoard = board.getBoardStructure();
        Node[][] cloneBoardStructure = cloneBoard.getBoardStructure();

        for (int i = 0; i < boardRow; i++) {
            for (int j = 0; j < boardColumn; j++) {
                if (originalBoard[i][j] != null) {
                    int originalNodeType = originalBoard[i][j].getType();
                    Constant originalNodeAction = originalBoard[i][j].getAction();
                    // Clone
                    cloneBoardStructure[i][j] = new Node(originalNodeType, originalNodeAction);
                }
            }
        }
        return cloneBoard;
    }

    /**
     * fill board by condition
     * fill with Node if false, null is true
     * @param board
     * @param fillNull
     */
    private void fillBoardByCondition(Board board, boolean fillNull) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();
        Node[][] boardStructure = board.getBoardStructure();

        if (fillNull) {
            for (Node[] subBoard : board.getBoardStructure())
                Arrays.fill(subBoard, null);
        } else {
            for (int i = 0; i < boardRow; i++) {
                for (int j = 0; j < boardColumn; j++) {
                    Set<Integer> unwantedSet = this.generateUnwantedRandomSet(board, i, j);
                    int type = ThreadLocalRandom.current().nextInt(1, 7);
                    while (unwantedSet.contains(type))
                        type = ThreadLocalRandom.current().nextInt(1, 7);
                    boardStructure[i][j] = new Node(type);
                }
            }
        }
    }

    /**
     * Clean board of blank nodes vertically
     * @param board
     * @return
     */
    private List<List<Node>> getAllNodes(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();

        Node[][] boardStructure = board.getBoardStructure();
        List<List<Node>> nonBlankMatrix = new ArrayList<>();

        for (int i = 0; i < boardColumn; i++) {
            List<Node> nonBlankList = new ArrayList<>();
            for (int j = 0; j < boardRow; j++) {
                if (boardStructure[j][i] != null)
                    nonBlankList.add(boardStructure[j][i]);
            }
            nonBlankMatrix.add(nonBlankList);
        }
        return nonBlankMatrix;
    }

    /**
     * Move board
     * @param board
     * @param nonBlankMatrix
     */
    private void moveBoard(Board board, List<List<Node>> nonBlankMatrix) {
        int boardRow = board.getNoOfRow();
        Node[][] boardStructure = board.getBoardStructure();

        int i = 0;
        for (List<Node> nonBlankList : nonBlankMatrix) {
            Collections.reverse(nonBlankList);
            int j = boardRow-1;

            for (Node content : nonBlankList) {
                boardStructure[j][i] = content;
                j--;
            }
            i++;
        }
    }

    /**
     * Check game over by swapping horizontally
     * @param board
     * @return
     */
    @SuppressWarnings("Duplicates")
    private boolean isGameOverHorizontal(Board board) {
        for (int i = 0; i < board.getNoOfRow(); i++) {
            for (int j = 0; j < board.getNoOfColumn()-1; j++) {
                Board throwawayBoard = this.cloneBoard(board);
                this.switchNode(throwawayBoard, i, j, i, j+1);

                if (this.canDelete(throwawayBoard)) return false;
            }
        }
        return true;
    }

    /**
     * Check game over by swapping vertically
     * @param board
     * @return
     */
    @SuppressWarnings("Duplicates")
    private boolean isGameOverVertical(Board board) {
        for (int i = 0; i < board.getNoOfRow()-1; i++) {
            for (int j = 0; j < board.getNoOfColumn(); j++) {
                Board throwawayBoard = this.cloneBoard(board);
                this.switchNode(throwawayBoard, i, j, i+1, j);

                if (this.canDelete(throwawayBoard)) return false;
            }
        }
        return true;
    }

    /**
     * Check if board can delete
     * @param board
     * @return
     */
    public boolean canDelete(Board board) {
        this.scanBoard(board);
        for (Node[] subBoard: board.getBoardStructure()) {
            for(Node i: subBoard) {
                if (i.getAction() == Constant.ACTION_DELETE) return true;
            }
        }
        return false;
    }

    /**
     * Generate unwanted random set for board spawn
     * Shut up it works
     * @param board
     * @param row
     * @param column
     * @return
     */
    private Set<Integer> generateUnwantedRandomSet(Board board, int row, int column){
        Node[][] boardStructure = board.getBoardStructure();
        Set<Integer> unwantedSet = new HashSet<>();

        try {
            if (boardStructure[row][column + 1].getType() == boardStructure[row][column - 1].getType())
                unwantedSet.add(boardStructure[row][column + 1].getType());
        } catch (Exception e) {}
        try {
            if (boardStructure[row][column + 1].getType() == boardStructure[row][column + 2].getType())
                unwantedSet.add(boardStructure[row][column + 1].getType());
        } catch (Exception e) {}
        try {
            if (boardStructure[row][column - 1].getType() == boardStructure[row][column - 2].getType())
                unwantedSet.add(boardStructure[row][column - 1].getType());
        } catch (Exception e) {}
        try {
            if (boardStructure[row + 1][column].getType() == boardStructure[row - 1][column].getType())
                unwantedSet.add(boardStructure[row + 1][column].getType());
        } catch (Exception e) {}
        try {
            if (boardStructure[row + 1][column].getType() == boardStructure[row + 2][column].getType())
                unwantedSet.add(boardStructure[row + 1][column].getType());
        } catch (Exception e) {}
        try {
            if (boardStructure[row - 1][column].getType() == boardStructure[row - 2][column].getType())
                unwantedSet.add(boardStructure[row - 1][column].getType());
        } catch (Exception e) {}

        return unwantedSet;
    }
}
