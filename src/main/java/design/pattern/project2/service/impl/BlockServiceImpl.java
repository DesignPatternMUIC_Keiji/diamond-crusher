package design.pattern.project2.service.impl;

import design.pattern.project2.model.Block;
import design.pattern.project2.model.Board;
import design.pattern.project2.model.Node;
import design.pattern.project2.service.BlockService;

import java.util.*;

/**
 * Created by Trung on 10/6/2017.
 */
public class BlockServiceImpl implements BlockService{

    /**
     * Generate block objects for animation
     * @param board
     * @return
     */
    @Override
    public void generateBlock(Board board, boolean isSpawn) {
        int boardRow = board.getNoOfRow();
        Map<Integer, List<Block>> blockMap = new HashMap<>();

        for (int i = 0; i < boardRow; i++) {
            List<Block> blockList;
            if (!isSpawn)
                blockList = this.generateBlockByColumn(board, i);
            else
                blockList = this.generateSpawnBlockByColumn(board, i);

            // Put to map only if blocks exist
            if (blockList.size() > 0)
                blockMap.put(i, blockList);
        }
        board.setBlockMap(blockMap);
    }

    /**
     * Generate block by rowIndex
     * Assuming that Node order in block isn't important
     * @param board
     * @param rowIndex
     * @return
     */
    private List<Block> generateBlockByColumn(Board board, int rowIndex) {
        int boardColumn = board.getNoOfColumn();
        Node[][] boardStructure = board.getBoardStructure();
        List<Block> blockList = new ArrayList<>();

        int lowerBound = 0;
        int upperBound = 0;
        boolean foundLowerbound = false;
        boolean foundUpperbound = false;
        List<int[]> nodeList = new ArrayList<>();

        for (int i = boardColumn-1; i >= 0; i--) {
            if (foundLowerbound) {
                // Finish with 1 block
                if ((boardStructure[i][rowIndex] == null && nodeList.size() > 0)) {
                    blockList.add(new Block(nodeList, lowerBound - upperBound));
                    lowerBound -= nodeList.size();
                    foundUpperbound = false;
                    nodeList = new ArrayList<>();
                }
                // Continue adding to block
                if (boardStructure[i][rowIndex] != null) {
                    // Found upperbound for drop distance
                    if (!foundUpperbound) {
                        upperBound = i;
                        foundUpperbound = true;
                    }
                    int[] nodePosition = {i + (lowerBound - upperBound), rowIndex};
                    nodeList.add(nodePosition);
                }

                // Finish with column
                if (i == 0 && foundUpperbound) blockList.add(new Block(nodeList, lowerBound - upperBound));
            }
            // Found lowerbound for drop distance
            else if (boardStructure[i][rowIndex] == null){
                lowerBound = i;
                foundLowerbound = true;
            }
        }
        return blockList;
    }

    /**
     * Generate spawn block
     * @param board
     * @param rowIndex
     * @return
     */
    private List<Block> generateSpawnBlockByColumn(Board board, int rowIndex) {
        int boardColumn = board.getNoOfColumn();
        Node[][] boardStructure = board.getBoardStructure();
        List<Block> blockList = new ArrayList<>();

        int dropDistance = 0;
        boolean foundLowerbound = false;
        List<int[]> nodeList = new ArrayList<>();

        for (int i = boardColumn-1; i >= 0; i--) {
            if (foundLowerbound) {
                int[] nodePosition = {i, rowIndex};
                nodeList.add(nodePosition);
                if (i == 0) blockList.add(new Block(nodeList, dropDistance));
            }
            // Found lowerbound for drop distance
            else if (boardStructure[i][rowIndex] == null){
                int[] nodePosition = {i, rowIndex};
                nodeList.add(nodePosition);
                dropDistance = i + 1;
                foundLowerbound = true;
                if (i == 0) blockList.add(new Block(nodeList, dropDistance));
            }
        }
        return blockList;
    }
}
