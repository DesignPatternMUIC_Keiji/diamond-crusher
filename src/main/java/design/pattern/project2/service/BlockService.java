package design.pattern.project2.service;

import design.pattern.project2.model.Board;

/**
 * Created by Trung on 10/6/2017.
 */
public interface BlockService {

    /**
     * Generate block for fall animation
     * @param board
     * @param isSpawn
     * @return
     */
    void generateBlock(Board board, boolean isSpawn);
}
