package design.pattern.project2.service;

import design.pattern.project2.model.Board;
import design.pattern.project2.model.Command;

import java.util.List;

/**
 * Created by Trung on 10/8/2017.
 */
public interface GameService {

    /**
     *  @param board
     * @param row1
     * @param column1
     * @param row2
     * @param column2
     */
    List<Command> switchNode(Board board, int row1, int column1, int row2, int column2);
}
