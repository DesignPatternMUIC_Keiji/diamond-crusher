package design.pattern.project2.service;

public interface DimensionCalculationService {
    /**
     * Calculate the dimensions of the mainFrame
     * @return
     */
    int[] calculateMainFrameDimensions(int rows, int columns);

    /**
     * Calculate the dimensions of the GamePanel in the mainFrame
     * @return
     */
    int[] calculateGamePanelDimensions(int rows, int columns);

    /**
     * just return the size of the menu bar
     * @return
     */
    int[] calculateMenuBarDimensions(int columns);

    int[] calculateScorePanelDimensions();

    int[] calculateTimerDimensions();

    int[] calculateOffSet(int rowNum, int colNum);
}
