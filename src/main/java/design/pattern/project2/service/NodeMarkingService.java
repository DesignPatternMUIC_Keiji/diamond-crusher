package design.pattern.project2.service;

import design.pattern.project2.model.Board;

/**
 * Created by Trung on 10/5/2017.
 */
public interface NodeMarkingService {
    /**
     * Mark nodes if there are 3 or more of the same type
     * @param board
     * @param row
     * @param column
     */
    void markNode(Board board, int row, int column);

    /**
     * Mark nodes passing through bombs
     * @param board
     * @param row
     * @param column
     */
    void markBombNode(Board board, int row, int column);
}
