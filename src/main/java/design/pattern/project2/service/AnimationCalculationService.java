package design.pattern.project2.service;

import design.pattern.project2.gui.component.GameLabel;
import design.pattern.project2.gui.component.GamePanel;
import design.pattern.project2.model.Block;

import java.util.concurrent.CountDownLatch;

public interface AnimationCalculationService {
    /**
     * Given a position and the board, do a delete on that position
     */
    void deleteAPosition(GamePanel gamePanel, CountDownLatch countDownLatch, Integer[] n1);

    /**
     * Given 2 position, do a swapping of the two game label
     * @param gamePanel
     * @param n1
     * @param n2
     */
    void swapPositions(GamePanel gamePanel, CountDownLatch countDownLatch, Integer[] n1, Integer[] n2);

    /**
     * Given a bunch of positions, move all of them down
     * Basically, each list of Integer[] represent a row to move first, then move next and then move next
     * @param gamePanel
     */
    void moveBlockDown(GamePanel gamePanel, CountDownLatch countDownLatch, Block node);

    /**
     * Basically like moveBlockDown, but now we need to create the gameLabel
     * @param gamePanel
     * @param countDownLatch
     * @param node
     */
    void spawnBlock(GamePanel gamePanel, CountDownLatch countDownLatch, Block node);

    void move(GameLabel gameLabel, Integer[] position);

}
