package design.pattern.project2.service;

import design.pattern.project2.model.Board;

/**
 * Created by Trung on 10/5/2017.
 */
public interface BoardCheckService {

    /**
     * Check for 3 or more nodes of the same type in row
     * @param row is specific given row, not max row
     * @param column similarly
     * @return
     */
    boolean checkHorizontal(Board board, int row, int column);

    /**
     * Check for 3 or more nodes of the same type in column
     * @param row is specific given row, not max row
     * @param column similarly
     * @return
     */
    boolean checkVertical(Board board, int row, int column);

    /**
     * Check if switch is valid
     * @param row1
     * @param column1
     * @param row2
     * @param column2
     * @return
     */
    boolean isValidSwitch(int row1, int column1, int row2, int column2);

    /**
     * Check if switching bomb
     * @param board
     * @param row1
     * @param column1
     * @param row2
     * @param column2
     * @return
     */
    int[] checkBombBySwitchedNodes(Board board, int row1, int column1, int row2, int column2);

    /**
     * Check if board contains bomb
     * @param board
     * @return
     */
    boolean isBoardContainsBomb(Board board);
}
