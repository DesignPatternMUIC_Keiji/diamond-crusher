package design.pattern.project2.service;

import design.pattern.project2.model.Board;

/**
 * Created by Trung on 10/1/2017.
 */
public interface BoardService {

    /**
     * Set board variables
     * @param board
     */
    void setBoard(Board board);

    /**
     * Spawn content into board
     * @param board
     */
    int spawnBoardNode(Board board);

    /**
     * Swap node
     * @param board
     * @param row1
     * @param column1
     * @param row2
     * @param column2
     */
    void switchNode(Board board, int row1, int column1, int row2, int column2);

    /**
     * Do move down after deletion
     * @param board
     */
    void moveDown(Board board);

    /**
     * Scan board for delete-able nodes
     * @param board
     */
    void scanBoard(Board board);

    /**
     * Scan board for delete-able nodes by bomb
     * @param board
     * @param row
     * @param column
     */
    void scanBoard(Board board, int row, int column);

    /**
     * Delete marked nodes
     * @param board
     */
    void deleteNode(Board board);

    /**
     * Check for game over status
     * @param board
     * @return
     */
    boolean checkGameOverStatus(Board board);

    /**
     * Covert all the properties of the board into int[][] so that the view can properly render it
     * @param board
     * @return
     */
    int[][] translateBoard(Board board);

    /**
     * Clone board
     * @param board
     * @return
     */
    Board cloneBoard(Board board);

    /**
     * Check if board can delete
     * @param board
     * @return
     */
    boolean canDelete(Board board);
}
