package design.pattern.project2.gui.component;

import javax.swing.*;
import java.awt.*;

public class ScorePanel extends JPanel {

    JLabel scoreBoard;
    int score;
    
    public ScorePanel(int width, int height){
        this.setSize(width,height);
        this.setLayout(new FlowLayout());
        score = 0;
        scoreBoard = new JLabel("Score\n" + score);
        this.add(scoreBoard);
    }

    public void update(int addedScore){
        score += addedScore;
        scoreBoard.setText("Score\n" + score);
    }

    public int getScore() {
        return score;
    }
}
