package design.pattern.project2.gui.component;

import design.pattern.project2.Worker.TimeWorker;

import javax.swing.*;

public class TimerPanel extends JPanel {

    JLabel timer;
    TimeWorker timeWorker;

    public TimerPanel(int width, int height){
        timer = new JLabel();
        this.setSize(width,height);
        this.add(timer);
        timeWorker = new TimeWorker(timer);
        timeWorker.execute();
    }

    public TimeWorker getTimeWorker() {
        return timeWorker;
    }
}
