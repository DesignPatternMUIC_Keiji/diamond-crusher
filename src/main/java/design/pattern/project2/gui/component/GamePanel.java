package design.pattern.project2.gui.component;

import design.pattern.project2.constant.View;
import design.pattern.project2.listener.AnimationListener.HoverListener;
import design.pattern.project2.model.ClickCounter;
import design.pattern.project2.service.DimensionCalculationService;
import design.pattern.project2.service.impl.DimensionCalculationServiceImpl;

import javax.swing.*;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GamePanel  extends JPanel {


    private int rows;
    private int columns;
    private boolean animating;
    private DimensionCalculationService calculationService = new DimensionCalculationServiceImpl();
    private GameLabel[][] renderedBoard;
    private ClickCounter clickCounter;
    private List<Integer[]> positionsClicked;


    public GamePanel(int rows, int columns, ClickCounter clickCounter){
        super();
        this.clickCounter= clickCounter;
        this.rows  = rows;
        this.columns = columns;
        renderedBoard = new GameLabel[rows][columns];
        this.setLayout(null);
        this.positionsClicked = new ArrayList<>();
        setUp();
    }

    private void setUp(){
        int[] dimensions = calculationService.calculateGamePanelDimensions(this.rows,this.columns);
        this.setSize(dimensions[0],dimensions[1]);
        this.setPreferredSize(new Dimension(dimensions[0],dimensions[1]));

        for (int i = 0; i < this.rows; i ++){
            for (int j = 0; j < this.columns; j++){
                Integer[] pos = new Integer[2];
                pos[0] = i;
                pos[1] = j;
                int[] offsets = calculationService.calculateOffSet(i,j);
                GameLabel gameLabel = spawnGameLabel(pos);
                gameLabel.setBounds(offsets[0],offsets[1], View.COLUMN_SIZE,View.ROW_SIZE);
                gameLabel.repaint();
            }
        }
        this.doLayout();
        this.repaint();
    }

    public void render(int[][] board){
        for (int i = 0; i < this.rows; i ++){
            for (int j = 0; j < this.columns; j++) {
                this.renderedBoard[i][j].setValue(board[i][j]);
                this.renderedBoard[i][j].repaint();
            }
        }
        this.doLayout();
        this.repaint();
    }

    public void addPositionClicked(Integer[] position) {
        this.positionsClicked.add(position);
    }
    public void removePositionClicked(Integer[] position){
        for (Iterator<Integer[]> iterator = this.positionsClicked.iterator(); iterator.hasNext();) {
            Integer[] pos = iterator.next();
            if (pos[0].equals(position[0]) && pos[1].equals(position[1])){
                iterator.remove();
                break;
            }
        }
    }

    public void resetClicks(){
        for (Integer[] pos: positionsClicked){
            renderedBoard[pos[0]][pos[1]].resetClick();
        }
        this.positionsClicked = new ArrayList<>();
        this.repaint();
    }

    public void destroyAPosition(Integer[] n){
        renderedBoard[n[0]][n[1]] = null;
    }

    public void swap(Integer[] n1, Integer[] n2){
        GameLabel tmp = getGameLabelInThatPosition(n1);
        renderedBoard[n1[0]][n1[1]] = getGameLabelInThatPosition(n2);
        renderedBoard[n2[0]][n2[1]] = tmp;
        if (renderedBoard[n1[0]][n1[1]] != null){
            renderedBoard[n1[0]][n1[1]].setRowNo(n1[0]);
            renderedBoard[n1[0]][n1[1]].setColumnNo(n1[1]);
        }
        if (renderedBoard[n2[0]][n2[1]] != null){
            renderedBoard[n2[0]][n2[1]].setRowNo(n2[0]);
            renderedBoard[n2[0]][n2[1]].setColumnNo(n2[1]);
        }
    }

    public void printBoard(){
        for (int i = 0; i < rows; i++){
            for (int j = 0; j < columns; j++){
                if (renderedBoard[i][j] != null){
                    System.out.print("1 ");
                }else{
                    System.out.println("0 ");
                }

            }
            System.out.println();
        }
    }

    public GameLabel spawnGameLabel(Integer[] n){
        renderedBoard[n[0]][n[1]] = new GameLabel(n[0],n[1]);
        renderedBoard[n[0]][n[1]].setSize(View.COLUMN_SIZE,View.ROW_SIZE);
        renderedBoard[n[0]][n[1]].setPreferredSize(new Dimension(View.COLUMN_SIZE,View.ROW_SIZE));
        this.add(renderedBoard[n[0]][n[1]]);
        renderedBoard[n[0]][n[1]].addMouseListener(new HoverListener(clickCounter, this ,renderedBoard[n[0]][n[1]]));
        return renderedBoard[n[0]][n[1]];
    }

    public GameLabel spawnGameLabel(int r, int c){
        renderedBoard[r][c] = new GameLabel(r,c);
        renderedBoard[r][c].setSize(View.COLUMN_SIZE,View.ROW_SIZE);
        renderedBoard[r][c].setPreferredSize(new Dimension(View.COLUMN_SIZE,View.ROW_SIZE));
        this.add(renderedBoard[r][c]);
        renderedBoard[r][c].addMouseListener(new HoverListener(clickCounter, this ,renderedBoard[r][c]));
        return renderedBoard[r][c];
    }

    public void setAnimating(boolean animating) {
        this.animating = animating;
    }

    public boolean isAnimating() {
        return animating;
    }

    /**
     * usually use only in animation
     * @param n
     * @return
     */
    public GameLabel getGameLabelInThatPosition(Integer[] n){
        System.out.println(n[0] + " " + n[1]);
        return this.renderedBoard[n[0]][n[1]];
    }
    public GameLabel getGameLabelInThatPosition(int[] n){
        return this.renderedBoard[n[0]][n[1]];
    }

    public GameLabel getGameLabelInThatPosition(int r, int c){
        return this.renderedBoard[r][c];
    }

    /**
     * return: {[i,j],[i,j]}
     * */
    public List<Integer[]> getPositionsClicked(){
        return this.positionsClicked;
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }
}
