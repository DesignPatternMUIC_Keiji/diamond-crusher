package design.pattern.project2.gui.component;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.util.HashMap;

/**
 * Given int, render the proper color
 * have an onclick to add on boarder
 */
public class GameLabel extends JLabel{
    int value;
    int rowNo;
    int columnNo;
    boolean isClicked;
    final static HashMap<Integer,Color> COLOR_HASH_MAP = createColorHashMap();

    public GameLabel(int r,int c){
        this.rowNo = r;
        this.columnNo = c;
        this.setOpaque(true);
        this.setVerticalAlignment(SwingConstants.CENTER);
        this.setHorizontalAlignment(SwingConstants.CENTER);
        this.isClicked = false;
    }

    //
    public void resetClick(){
        this.setBorder(null);
        this.setIsClicked(false);
        this.setNotHovered();
    }


    //The only setter method that matters
    public void setValue(int value){
        this.value = value;
//        System.out.println(value);
        this.setBackground(COLOR_HASH_MAP.get(this.value));
        this.setForeground(COLOR_HASH_MAP.get(this.value));
        this.setText(" ");
        this.setBorder(new LineBorder(Color.GRAY, 2));
        this.repaint();
        this.doLayout();
    }

    public void setHovered(){
        if (!this.isClicked)
            this.setBorder(new LineBorder(Color.YELLOW,5));
    }

    public void setNotHovered(){
        if (!this.isClicked){
            this.setBorder(null);
            this.setBorder(new LineBorder(Color.GRAY,2));
        }
    }

    public void setClicked(){
        if (!this.isClicked) {
            this.setBorder(new LineBorder(Color.YELLOW, 8));
            setIsClicked(true);
        }
        else{
            setIsClicked(false);
            setNotHovered();
            setHovered();
        }
    }

    public boolean isClicked() {
        return isClicked;
    }

    public Integer[] getPosition(){
        Integer[] n = new Integer[2];
        n[0] = rowNo;
        n[1] = columnNo;
        return n;
    }

    public int getValue() {
        return value;
    }

    public boolean isDestroyed(){
        Rectangle rectangle = this.getBounds();
        if (rectangle.getX() <= 0 && rectangle.getY() <= 0){
            return true;
        }
        return false;
    }

    public void setBounds(Integer[] boundXY, Integer[] size) {
        super.setBounds(boundXY[0],boundXY[1],size[0],size[1]);
        this.repaint();
    }

    public void setRowNo(int rowNo) {
        this.rowNo = rowNo;
    }

    public void setColumnNo(int columnNo) {
        this.columnNo = columnNo;
    }

    public void setClicked(boolean clicked) {
        isClicked = clicked;
    }

    public void setIsClicked(boolean t){
        isClicked = t;
    }

    static private HashMap<Integer,Color> createColorHashMap(){
        HashMap<Integer,Color> colorHashMap = new HashMap<>();
        colorHashMap.put(1, Color.MAGENTA);
        colorHashMap.put(2, Color.GREEN);
        colorHashMap.put(3, Color.CYAN);
        colorHashMap.put(4, Color.red);
        colorHashMap.put(5, Color.ORANGE);
        colorHashMap.put(6, Color.BLUE);
        // bomb
        colorHashMap.put(7, Color.black);
        return colorHashMap;
    }
}
