package design.pattern.project2.model;

import design.pattern.project2.constant.Constant;

/**
 * Created by Trung on 10/1/2017.
 */
public class Node {

    int type;
    Constant action;

    public Node(int type, Constant action) {
        this.type = type;
        this.action = action;
    }

    public Node(int type) {
        this.type = type;
        this.action = null;
    }

    public int getType() {
        return type;
    }

    public Constant getAction() {
        return action;
    }

    public void setAction(Constant action) {
        this.action = action;
    }
}
