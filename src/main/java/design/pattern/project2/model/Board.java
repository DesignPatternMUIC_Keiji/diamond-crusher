package design.pattern.project2.model;

import design.pattern.project2.service.BoardService;
import design.pattern.project2.service.impl.BoardServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * Created by Trung on 10/1/2017.
 */

public class Board {

    private int row;
    private int column;
    private Map<Integer, List<Block>> blockMap;
    private Node[][] boardStructure;
    private boolean isGameOver = false;

    BoardService boardService = new BoardServiceImpl();

    public Board(int row, int column) {
        this.row = row;
        this.column = column;
        this.boardStructure = new Node[row][column];
        boardService.setBoard(this);
    }

    public int getNoOfRow() {
        return row;
    }

    public int getNoOfColumn() {
        return column;
    }

    public Node[][] getBoardStructure() {
        return boardStructure;
    }

    public void setBoardStructure(Node[][] boardStructure) {
        this.boardStructure = boardStructure;
    }

    public boolean isGameOver() {
        return isGameOver;
    }

    public void setGameOver(boolean gameOver) {
        isGameOver = gameOver;
    }

    public Map<Integer, List<Block>> getBlockMap() {
        return blockMap;
    }

    public void setBlockMap(Map<Integer, List<Block>> blockMap) {
        this.blockMap = blockMap;
    }
}

