package design.pattern.project2.model;

import java.util.List;

/**
 * Created by Trung on 10/6/2017.
 */
public class Block {

    List<int[]> block;
    int dropDistance;

    public Block(List<int[]> block, int dropDistance) {
        this.block = block;
        this.dropDistance = dropDistance;
    }

    public List<int[]> getBlock() {
        return block;
    }

    public int getDropDistance() {
        return dropDistance;
    }

}
