package design.pattern.project2.model;

import design.pattern.project2.constant.ComponentName;
import design.pattern.project2.singleton.EventBus;

import java.util.Observable;

public class ClickCounter extends Observable{
    private int numberOfItemsClicked = 0;

    public  void increment(){
        numberOfItemsClicked++;
        if (numberOfItemsClicked == 2){
            EventBus.notify(ComponentName.SWAP,null);
            reset();
        }

    }

    public  void decrement(){
        numberOfItemsClicked--;
    }

    private   void reset(){
        numberOfItemsClicked = 0;
    }

    public  int get(){
        return numberOfItemsClicked;
    }

    private void broadcast(){
        setChanged();
        notifyObservers();
    }
}
