package design.pattern.project2.model;

import design.pattern.project2.constant.Constant;

/**
 * Created by Trung on 10/8/2017.
 */
public class Command {

    private Board board;
    private Constant action;

    public Command(Board board, Constant action) {
        this.board = board;
        this.action = action;
    }

    public Board getBoard() {
        return board;
    }

    public Constant getAction() {
        return action;
    }
}
