package design.pattern.project2.constant;

/**
 * Created by Trung on 10/5/2017.
 */
public enum Constant {

    ACTION_DELETE("delete"),
    ACTION_SWITCH("switch"),
    ACTION_SPAWN("spawn"),
    ACTION_MOVE("move"),
    ACTION_COMPLETE("complete");

    private String value;

    Constant(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
