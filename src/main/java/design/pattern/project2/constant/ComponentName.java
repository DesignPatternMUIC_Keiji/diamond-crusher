package design.pattern.project2.constant;

public class ComponentName {
    public static final String GAMEPANEL = "GAMEPANEL";
    public static final String MAINFRAME = "MAINFRAME";
    public static final String TIMERPANEL = "GAMEPANEL";
    public static final String SCOREPANEL = "GAMEPANEL";
    public static final String NEWGAME = "NEWGAME";
    public static final String CUSTOMIZEGAME = "CUSTOMIZEGAME";
    public static final String SWAP = "SWAP";
    public static final String ANIMATE = "ANIMATE";
    public static final String STOP = "STOP";
    public static final String GAMEOVER = "GAMEOVER";
    public static final String ADDSCORE = "ADDSCORE";
}
