package design.pattern.project2.singleton;

import design.pattern.project2.listener.EventBusListener.GenericListener;

import java.util.*;

public class EventBus {

    private static List<GenericListener> listenerList = new ArrayList<>();


    public static void register(GenericListener genericListener){
        listenerList.add(genericListener);
    }

    public static void unregister(String componentName){
        for (Iterator<GenericListener> iterator = listenerList.iterator(); iterator.hasNext();){
            GenericListener genericListener = iterator.next();
            if (genericListener.getComponentName().equals(componentName)){
                iterator.remove();
            }
        }
    }

    public static void notify(String componentName, Object obj){
        for (Iterator<GenericListener> iterator = listenerList.iterator(); iterator.hasNext();){
            GenericListener genericListener = iterator.next();
            if (genericListener.getComponentName().equals(componentName)) {
                genericListener.update(obj);
                break;
            }
        }
    }

}
